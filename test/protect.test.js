//Evaluate broad functionality of protect module
'use-strict';

const protect = require('../src/protect.js');


function doubleNum(x) {
  return x * 2;
}


describe('Test buildFunc', () => {
  test('Build doubleNum', () => {
    const argSpec = [
      {
        name: 'x',
        type: 'number',
      },
    ];
    const retSpec = {
      type: 'number',
    };
    const built = protect.buildFunc(doubleNum, argSpec, retSpec);
    expect(built(3)).toBe(6);
    expect(built(-9.5)).toBe(-19);
    expect(() => built('1')).toThrow(/received.*string.*expected.*number/i);
  });
});
