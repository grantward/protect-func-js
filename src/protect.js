//Provides runtime type checking of function arguments and return values
//Also builds documentation of built functions


function buildFunc(func, argumentsDescription, retDescription, docString) {
  function built() {
    let args = [];
    for (const argDescIdx in argumentsDescription) {
      const argDesc = argumentsDescription[argDescIdx];
      const arg = arguments[argDescIdx];
      validateArg(arg, argDesc, argDescIdx);
      args.push(arg);
    }
    let res = func(args);
    validateRet(res);
    return res;
  }
  return built;
}

function validateArg(arg, argDesc) {
  if (!argDesc.hasOwnProperty('default')) {
    if (arg === undefined) {
      //Argument is required but not present
      //TODO: more specifics
      throw 'Argument is required but not present';
    }
  }
  if (argDesc.type) {
    typeMatch(arg, argDesc.type);
  }
}

function validateRet(ret, retDescription) {
  if (retDescription) {
    if (retDescription.type) {
      typeMatch(ret, retDescription.type);
    }
  }
}

function typeMatch(received, expected) {
  const recType = typeof received;
  const expType = typeof expected;
  if (expType === 'string') {
    if (!(recType === expected)) {
      throw `Wrong type: received: ${recType} expected: ${expected}`;
    }
  } else {
    //TODO: check recursively
  }
}


module.exports = {
  buildFunc,
};
